import { Component, OnInit } from '@angular/core';
import { User } from '../models/sers.models';

import { UserService } from '../user.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {


listUsers :User[]  |  undefined;

  constructor(private _userService: UserService) {}

  ngOnInit(): void {
    this._userService.GetUsers().subscribe({
      next: (x) =>  {
        this.listUsers=x
      }

    })


  }
}
