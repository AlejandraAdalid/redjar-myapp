import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
    {
        path: 'users',
        loadChildren: () => import("../app/users/users.module").then(m => m.UsersModule)
    },
    {
      path: 'albums',
      loadChildren: () => import("../app/albums/albums.module").then(m => m.AlbumsModule)
  }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
